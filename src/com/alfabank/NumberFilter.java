package com.alfabank;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class NumberFilter {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.print("Error occurred");
            return;
        }
        List<Integer> numbers = getNumbers(args[0]);
        if (numbers == null) {
            System.out.print("Error occurred");
        return;
        }

        java.util.Collections.sort(numbers);

        System.out.print("ASC: ");
        for (Integer number : numbers) {
            System.out.print(number + " ");
        }

        ListIterator listIterator = numbers.listIterator(numbers.size());
        System.out.println();
        System.out.print("DESC: ");
        while(listIterator.hasPrevious()) {
            System.out.print(listIterator.previous() + " ");
        }

    }

    public static List<Integer> getNumbers(String sourceFile) {
        List<Integer> numbers = new LinkedList<Integer>();
        try (BufferedReader stringReader = checkSourceFile(sourceFile);) {
            if (stringReader == null)
                return null;

            String inputString = null;
            inputString = stringReader.readLine();

            //String[] strings = inputString.split(",");
            for (String str : inputString.split(",")) {
                numbers.add(Integer.parseInt(str));
            }



        } catch (Exception e) {
            return null;
        }
        return numbers;
    }

    private static BufferedReader checkSourceFile(String sourceFile) {
        File file = new File(sourceFile);
        if (file.exists() == false)
            return null;
        else {
            try {
                return new BufferedReader(new FileReader(sourceFile));
            } catch (Exception e) {
                return null;
            }
        }

    }

}
